<?php
/*
Plugin Name: HC Telaraña
Plugin URI: http://hackeocultural.org
Description: Captura información directamente desde un formulario web, que permita la participación directa y, eventualmente, la clasificación sistematizada del contenido.
Version: 0.5.20200516
Author: balamaqab
Author URI: http://gitlab.com/balamaqab
*/


require_once dirname(__FILE__) . '/libs/class-tgm-plugin-activation.php';
add_action('tgmpa_register', 'hc__register_required_plugins');

function hc__register_required_plugins()
{
	$plugins = array(
		array(
			'name'      => 'Advanced Custom Fields',
			'slug'      => 'advanced-custom-fields',
			'force_activation'   => true,

			'required'  => true,
		),

	);
	$config = array(
		'id'           => 'hc_telarana',
		'default_path' => '',
		'menu'         => 'tgmpa-install-plugins',
		'parent_slug'  => 'plugins.php',
		'capability'   => 'manage_options',
		'has_notices'  => true,
		'dismissable'  => false,
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa($plugins, $config);
}

// require_once dirname(__FILE__) . '/user-creation.php'; // Innecesario por ahora
require_once dirname(__FILE__) . '/post-save.php';
require_once dirname(__FILE__) . '/template-customization.php';


function hc_activate_hc_telarana()
{
	hc_prepare_user();
}
register_activation_hook(__FILE__, 'hc_activate_hc_telarana');
