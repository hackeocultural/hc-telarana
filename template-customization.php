<?php

function hc_telarana_replace_post_template($single_template)
{
    global $post;
    if (in_category('narrativas')) {
        $single_template = dirname(__FILE__) . '/templates/single-hackeo.php';
    } else if (in_category('alternativas')) {
        $single_template = dirname(__FILE__) . '/templates/single-hackeo.php';
    }
    return $single_template;
}
add_filter("single_template", "hc_telarana_replace_post_template");

function hc_telarana_replace_page_template($page_template)
{
    global $post;
    if (is_page('telarana')) {
        $page_template = dirname(__FILE__) . '/templates/page-telarana.php';
    }
    return $page_template;
}
add_filter("page_template", "hc_telarana_replace_page_template");
