<?php
add_action('acf/save_post', 'hc_telarana_save_post');
function hc_telarana_save_post($post_id)
{
    $values = get_fields($post_id);
    $post = get_post($post_id);

    $category = -1;

    if (!empty($values['post_category'])) {
        $cat = get_category_by_slug($values['post_category']);
    }

    if ($cat !== false) {
        $category = $cat->term_id;
    } else {
        $cat_narrativas = get_category_by_slug("narrativas");
        $category = $cat_narrativas->term_id;
    }

    $data_to_update =  array(
        'ID' => $post_id,
        'post_content' => $values["post_content"],
        'post_category' => [$category]
    );

    wp_update_post($data_to_update);
    wp_set_post_tags($post_id, $values["etiquetas"], true);
}
