<?php
function hc_create_user($user_name, $user_email){
	$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
	$user_id = wp_create_user( $user_name, $random_password, $user_email );
	return $user_id;
}

function hc_prepare_user(){

	$user_name = "pandemia";
	$user_email = "pandemia@hackeocultural.org";

	$user_id = username_exists( $user_name);

	if ( !$user_id and email_exists($user_email) == false ) {
		hc_create_user($user_name, $user_email);
	} 
}
?>