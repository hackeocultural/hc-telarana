<?php /* Template Name: Registro de narrativas y alternativas */ ?>
<?php
acf_form_head();
get_header();

?>
<div id="main-content">
    <div class="container">
        <div id="content-area" class="<?php extra_sidebar_class(); ?> clearfix">
            <div class="et_pb_extra_column_main">
                <?php
                if (have_posts()) :
                    while (have_posts()) : the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <div class="post-wrap">
                                <?php if (is_post_extra_title_meta_enabled()) { ?>
                                    <h1 class="entry-title"><?php the_title(); ?></h1>
                                <?php } ?>
                                <div class="post-content entry-content">

                                    <?php
                                    the_content();
                                    if (function_exists("acf_form")) {
                                    ?> <div style="margin:1em 2em;"><?php
                                                                    $field_group_id = false;
                                                                    $field_group = get_posts(array(
                                                                        'title'  => "Metadata de repositorio",
                                                                        'post_type'   => 'acf-field-group',
                                                                        'post_status' => 'publish',
                                                                        'numberposts' => 1
                                                                    ));

                                                                    if (isset($field_group[0]->ID)) {
                                                                        $field_group_id =  $field_group[0]->ID;
                                                                    }

                                                                    $update_message = "Información enviada exitosamente.";
                                                                    $submit_value = "Enviar";

                                                                    if (function_exists("get_field")) {
                                                                        $update_message = get_field("update_message");
                                                                        $submit_value = get_field("submit_value");
                                                                    }

                                                                    $post_status = "pending";

                                                                    if (is_user_logged_in()) {
                                                                        $user = wp_get_current_user();
                                                                        $roles = (array) $user->roles;
                                                                        if (current_user_can('publish_posts')) {
                                                                            $post_status = "publish";
                                                                        }
                                                                    }

                                                                    acf_form(array(
                                                                        'post_id'        => 'new_post',
                                                                        'post_title'    => true,
                                                                        'post_content'    => false,
                                                                        'submit_value' => $submit_value,
                                                                        'updated_message' => $update_message,
                                                                        'field_groups' => array($field_group_id),
                                                                        'new_post'        => array(
                                                                            'post_status'    => $post_status,
                                                                        )
                                                                    ));
                                                                }
                                                                    ?></div><?php
                                                                            ?>




                                        <?php
                                        if (!extra_is_builder_built()) {
                                            wp_link_pages(array(
                                                'before' => '<div class="page-links">' . esc_html__('Pages:', 'extra'),
                                                'after'  => '</div>',
                                            ));
                                        }
                                        ?>
                                </div>
                            </div><!-- /.post-wrap -->
                        </article>
                    <?php
                    endwhile;
                else :
                    ?>
                    <h2><?php esc_html_e('Post not found', 'extra'); ?></h2>
                <?php
                endif;
                wp_reset_query();
                ?>
                <?php
                if ((comments_open() || get_comments_number()) && 'on' == et_get_option('extra_show_pagescomments', 'on')) {
                    comments_template('', true);
                }
                ?>
            </div><!-- /.et_pb_extra_column.et_pb_extra_column_main -->

            <?php get_sidebar(); ?>

        </div> <!-- #content-area -->
    </div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer();
