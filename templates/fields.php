<?php
if (function_exists("get_fields")) {
    $fields = get_field_objects();
?>
    <ul class="content-card category-narrativa">
        <?php foreach ($fields as $field) :
            if (in_array($field['name'], array("author", "territorio_registrado", "momento"))) {
        ?>
                <li><strong><?php echo $field['label']; ?>:</strong> <?php echo $field['value']; ?></li>
            <?php
            } else if ($field['name'] === "url") { ?>
                <li><a href="<?php echo $field['value']; ?>">Visitar sitio web</a></li>
            <?php } else if ($field['name'] === "adjunto") { ?>
                <li><a href="<?php echo $field['value']; ?>">Descargar adjunto</a></li>
        <?php }
        endforeach;
        ?>
    </ul>
<?php } ?>